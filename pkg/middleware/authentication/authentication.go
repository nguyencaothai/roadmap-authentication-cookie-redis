package authentication

import (
	"context"
	"fmt"
	"net/http"

	"local.com/roadmap-redis/config"
	"local.com/roadmap-redis/internal/user"
)

func AuthMiddlware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		session, errSession := config.GetStore().Get(r, "session")
		if errSession != nil {
			fmt.Printf("error getting session\n %s", errSession)
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}
		if session.Values["authenticated"] == true {
			userID := session.Values["userID"].(int)

			ctx := context.WithValue(r.Context(), user.UserIDKey, userID)
			next.ServeHTTP(w, r.WithContext(ctx))
			return
		}

		http.Error(w, "Unauthorized", http.StatusUnauthorized)

	})
}
