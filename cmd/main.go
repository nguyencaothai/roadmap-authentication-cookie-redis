package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"local.com/roadmap-redis/config"
	"local.com/roadmap-redis/internal/secret"
	"local.com/roadmap-redis/internal/user"
	"local.com/roadmap-redis/pkg/middleware/authentication"
)

func main() {
	errLoadEnv := config.LoadEnv()
	if errLoadEnv != nil {
		fmt.Println(errLoadEnv)
	}

	errDatabaseInit := config.DatabaseInit()
	if errDatabaseInit != nil {
		fmt.Println(errDatabaseInit)
	}

	errMigrate := config.RunMigration()
	if errMigrate != nil {
		fmt.Println(errMigrate)
	}

	errSession := config.SessionInit()
	if errSession != nil {
		fmt.Println(errSession)
	}

	r := mux.NewRouter()

	userRoute := r.PathPrefix("/users").Subrouter()
	// userRoute.Use(authentication.AuthMiddlware)
	userRoute.HandleFunc("/login", user.LoginFunc).Methods("POST")

	secretRoute := r.PathPrefix("/secret").Subrouter()
	secretRoute.Use(authentication.AuthMiddlware)
	secretRoute.HandleFunc("/", secret.SecretFunc)

	errStart := http.ListenAndServe(":8081", r)
	if errStart != nil {
		fmt.Println("error starting server at port 8081")
	}

}
