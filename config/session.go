package config

import (
	"crypto/rand"
	"fmt"

	"github.com/no-src/redistore"
)

var (
	sessionStore *redistore.RediStore
)

func SessionInit() error {
	var errNew error

	sessionStore, errNew = redistore.NewRediStore(
		10,
		"tcp",
		"127.0.0.1:6379",
		"",
		generateKey(32),
		generateKey(32),
	)
	if errNew != nil {
		fmt.Printf("error creating session redis store\n %s", errNew)
		return errNew
	}

	return nil

}

func generateKey(length int) []byte {
	bytes := make([]byte, length)
	if _, errRead := rand.Read(bytes); errRead != nil {
		fmt.Printf("error generating key\n %s", errRead)
		return []byte{}
	}

	return bytes
}

func GetStore() *redistore.RediStore {
	return sessionStore
}
