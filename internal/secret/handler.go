package secret

import (
	"fmt"
	"net/http"

	"local.com/roadmap-redis/internal/user"
)

func SecretFunc(w http.ResponseWriter, r *http.Request) {
	userID := r.Context().Value(user.UserIDKey)

	fmt.Fprintf(w, "This is secret of user %d", userID)
}
