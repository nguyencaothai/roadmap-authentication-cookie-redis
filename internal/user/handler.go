package user

import (
	"encoding/json"
	"fmt"
	"net/http"

	"local.com/roadmap-redis/config"
)

func LoginFunc(w http.ResponseWriter, r *http.Request) {
	var userInput UserInput

	errDecode := json.NewDecoder(r.Body).Decode(&userInput)
	if errDecode != nil {
		fmt.Printf("error decoding user login input\n %s", errDecode)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	userID, errCheck := checkValidUser(userInput)
	if errCheck != nil {
		if errCheck.Error() == "unauthorized" {
			http.Error(w, "Unauthorized", http.StatusUnauthorized)
			return
		}

		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	session, errSession := config.GetStore().Get(r, "session")
	if errSession != nil {
		fmt.Printf("error getting session\n %s", errSession)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	session.Values["authenticated"] = true
	session.Values["userID"] = userID

	errSave := session.Save(r, w)
	if errSave != nil {
		fmt.Printf("error saving session\n %s", errSave)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, "/secret/", http.StatusFound)
}
