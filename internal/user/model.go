package user

type UserInput struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type User struct {
	ID         int    `json:"userID"`
	Username   string `json:"username"`
	Password   string `json:"password"`
	Created_at string `json:"created_at"`
}

type ContextKey string

var UserIDKey ContextKey = "userID"
