package user

import (
	"fmt"

	"local.com/roadmap-redis/config"
)

func queryUser(userInput UserInput) (User, error) {
	var user User
	query := "select * from users where username = $1"
	errQuery := config.GetDB().QueryRow(query, userInput.Username).Scan(&user.ID, &user.Username, &user.Password, &user.Created_at)
	if errQuery != nil {
		fmt.Printf("error querying user from database\n %s", errQuery)
		return User{}, errQuery
	}

	return user, nil
}
